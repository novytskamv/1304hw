package com.company.task4;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MyApp<T> {

  private List<String> list;

//  private Stream<String> stream;
//
  private MyApp() {
    this.list = new ArrayList<>();
  }

  public Long uniqueNumber(){
    return list.stream().flatMap(e-> Stream.of(e.split(" "))).distinct().count();
  }

  public List<String> sortedList(){
    return list.stream().flatMap(e-> Stream.of(e.split(" "))).distinct().
        sorted().collect(Collectors.toList());
  }

  public Map <String, Long> occurenceNumber(){
    return list.stream().flatMap(e-> Stream.of(e.split(" "))).collect(groupingBy(Function.identity(), counting()));
  }

  public Map <String, Long>  occurenceLetter(){
    return letterList().stream().filter(e -> !e.equals(e.toUpperCase())).collect(groupingBy(Function.identity(), counting()));
  }

  public List<String> letterList(){
    return list.stream().flatMap(e->Stream.of(e.split(""))).filter(e-> !e.equals(" ")).collect(Collectors.toList());
  }


  public static void main(String[] args) {

    MyApp<String> myApp = new MyApp<>();
    System.out.println("==========================================================");
    System.out.println("Please enter any number of lines and ENDS with empty line");
    System.out.println("==========================================================");
    while (true){
      String input = readString();
      if(!input.isEmpty()){
        myApp.list.add(input);
      }
      else {break;}
    }
    System.out.println("                    YOUR TEXT");
    System.out.println("============================================");
    System.out.println(myApp.list);
    System.out.println("============================================\n");

    System.out.println("(1) Number of UNIQUE words is "+myApp.uniqueNumber()+"\n");
    System.out.println("                        ***\n");
    System.out.println("(2) Sorted list of all unique words: " + myApp.sortedList()+"\n");
    System.out.println("                        ***\n");
    System.out.println("(3) Occurrence number of each word\n");
    myApp.occurenceNumber().forEach((k, v) -> System.out.println(String.format("%s ==>> %d", k, v)));
    System.out.println("                        ***\n");
    System.out.println("\n");
    System.out.println("(4) Occurrence number of each letter\n");
    System.out.println("               YOUR LETTERS");
    System.out.println("==================================================================");
    System.out.println(myApp.letterList());
    System.out.println("==================================================================\n");
    myApp.occurenceLetter().forEach((k, v) -> System.out.println(String.format("%s ==>> %d", k, v)));
    System.out.println("                        ***\n");
    System.out.println("                      THE END");

  }

  private static String readString(){
    Scanner scanner = new Scanner(System.in);
    return scanner.nextLine();
  }
}
