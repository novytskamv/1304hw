package com.company.task2;

public class Command1 implements Command {

  @Override
  public void execute(String s) {
    System.out.println("Invoked as object of command class with argument:** " + s +" **");
  }
}
