package com.company.task2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Invoker {

  public static void main(String[] args) {

    Command command1 = (s) -> {
      System.out.println("**Invoked Lambda command with argument:** " + s+ " **");
    };

    Command command2 = new Command() {
      @Override
      public void execute(String s) {
        System.out.println("**Invoked an anonymous class with argument:** " + s+ " **");
      }
    };

    Command command3 = NewClass::execute;

    Command command4 = new Command1();

    Map menu = new HashMap();
    menu.put("1", command1);
    menu.put("2", command2);
    menu.put("3", command3);
    menu.put("4", command4);

    while (true){
      System.out.println("=================================================");
      System.out.println("     Please enter № of command and argument");
      System.out.println("=================================================");
      String command = readString();
      String argument = readString();

      if(menu.containsKey(command)){
      Command c = (Command) menu.get(command);
      c.execute(argument);
      break;
      } else {
        System.out.println("=================================================");
        System.out.println("              Wrong command NAME: " + command);
        System.out.println("=================================================");
      }
    }
  }
    private static String readString()
  {
    Scanner scanner = new Scanner(System.in);
    return scanner.nextLine();
  }
}
