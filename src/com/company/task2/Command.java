package com.company.task2;

@FunctionalInterface
public interface Command {

  void execute(String s);

}
