package com.company.task3;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MyStream <T> {

  private List<Integer> integerList;

/**
 * First method - used generate();
 **/
  List <Integer> myMethod1(){
    Stream<Integer> stream =  Stream.generate(MyStream::random).limit(20);
    return this.integerList = stream.collect(Collectors.toList());
  }

  /**
   * Second method - used iterate();
   **/
  List<Integer> myMethod2(){
    Stream<Integer> stream = Stream.iterate(random(), n-> random()).limit(20);
    return this.integerList = stream.collect(Collectors.toList());
  }

  private static Integer random(){
    return (int)(Math.random()*100);
  }

  public static void main(String[] args) {

    MyStream<Integer> myStream = new MyStream<>();
    myStream.myMethod1();
    System.out.println("===============================================================================");
    System.out.println(myStream.integerList);
    System.out.println("===============================================================================");

    List<Integer> list = myStream.integerList;
    System.out.println("MAX (sum method) is "+ list.stream().max(Integer::compare).get());
    System.out.println("MIN (sum method) is "+list.stream().min(Integer::compare).get());
    System.out.println("SUM (sum method) is "+list.stream().collect(Collectors.summingInt(Integer::intValue)));
    System.out.println("AVG (sum method) is "+list.stream().collect(Collectors.averagingInt(Integer::intValue))+"\n");


    System.out.println("MAX (reduce method) is "+list.stream().mapToInt(i -> i).max().getAsInt());
    System.out.println("MIN (reduce method) is " +list.stream().mapToInt(i -> i).min().getAsInt());
    System.out.println("SUM (reduce method) is "+list.stream().mapToInt(i -> i).sum());
    System.out.println("AVG (reduce method) is "+list.stream().mapToDouble(d -> d).average().getAsDouble()+"\n");

    System.out.println("Number of values that are bigger than average is "
        +list.stream()
        .filter(v -> v > list.stream().collect(Collectors.averagingInt(Integer::intValue)))
        .count());
  }

}
