package com.company.task1;

@FunctionalInterface
public interface MyLambda {

  int function1(int a, int b, int c);

  public static void main(String[] args) {
    MyLambda myLambda1 = (a,b,c) -> {if(a>b&&a>c)
                                      return a;
                                    else if(a>b&&a<c)
                                      return c;
                                    else if(a<b&&a>c)
                                      return b;
                                    else if (a<b&&a<c&&b>c)
                                      return b;
                                    else return c;
    };

    MyLambda myLambda2 = (a,b,c) ->(a+b+c)/3;
    System.out.println("----------------------------------");
    System.out.println("MAX value is "+myLambda1.function1(10,11,8));
    System.out.println("AVG value is "+myLambda2.function1(3,5,6));
    System.out.println("----------------------------------");
  }

}
